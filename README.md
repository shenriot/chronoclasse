# Chronoclasse

**Chronoclasse** est une application web conçue pour aider les enseignants à gérer et visualiser le temps de parole des élèves en classe, lors de débats ou d'exposés. Grâce à cet outil, il est possible de suivre le temps de parole de chaque élève, de manière individuelle ou par équipes, favorisant ainsi une participation plus équitable et un environnement d'apprentissage inclusif.

## Fonctionnalités

- **Suivi en temps réel** : Permet de visualiser le temps de parole de chaque élève.
- **Participation équitable** : Encourage une participation équilibrée entre les élèves.
- **Mode équipes** : Compare les temps de parole entre différentes équipes.
- **Synthèse** : Génère des graphiques résumant le temps de parole à la fin d'une séance ou d'un débat.
- **Exportation des données** : Exportez les résultats pour un suivi ultérieur.

## Prérequis

- Navigateur web moderne (Chrome, Firefox, Edge).
- Aucune installation n'est requise, l'application est entièrement web.


## Licence

Ce projet est sous licence Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0). Cela signifie que vous êtes libre de partager et d'adapter le matériel pour des utilisations non commerciales, tant que vous créditez l'œuvre et que vous n'appliquez pas de restrictions supplémentaires. Voir le fichier [LICENSE](LICENSE) pour plus de détails.