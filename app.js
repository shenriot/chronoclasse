// app.js
let students = {};
let currentStudentId = null;
let chronoLance = false;  // Variable pour suivre l'état du chronomètre
let teamTimes = {};  // Pour suivre le temps total par équipe

// Liste de couleurs pour les équipes
const teamColors = ['bg-blue-200', 'bg-green-200', 'bg-yellow-200', 'bg-red-200', 'bg-purple-200', 'bg-pink-200'];

document.getElementById('generateButton').addEventListener('click', () => {
    // Si un chronomètre a été lancé, on ne génère plus d'avatars
    if (chronoLance) {
        alert("Vous ne pouvez plus générer de nouvelles équipes après avoir lancé un chronomètre.");
        return;
    }

    const avatarStyle = document.getElementById('avatarStyle').value; // Récupère le style d'avatar sélectionné
    const inputText = document.getElementById('nameInput').value; // Récupère le texte de la zone de saisie

    const teamContainers = document.getElementById('teamsContainer'); // Conteneur des équipes
    teamContainers.innerHTML = ''; // Vider le conteneur pour les nouvelles équipes

    // Séparer les différentes équipes par "/"
    const teams = inputText.split('/').map(team => team.trim());
    
    teams.forEach((team, teamIndex) => {
        if (team) {
            const teamId = `team-${teamIndex + 1}`;  // Générer un ID pour chaque équipe
            teamTimes[teamId] = 0;  // Initialiser le temps cumulé de l'équipe

            // Créer un conteneur pour chaque équipe avec une couleur de fond distincte
            const teamContainer = document.createElement('div');
            teamContainer.classList.add('team-container', teamColors[teamIndex % teamColors.length], 'p-4', 'rounded-lg', 'mb-4');
            teamContainer.id = teamId;

            const teamTitle = document.createElement('h2');
            teamTitle.classList.add('text-lg', 'font-bold', 'text-center', 'mb-4');
            teamTitle.textContent = `Équipe ${teamIndex + 1} - Temps cumulé : 00:00`;

            const membersContainer = document.createElement('div');
            membersContainer.classList.add('flex', 'flex-wrap', 'justify-center');

            team.split(',').forEach((name, index) => {
                name = name.trim();
                if (name) {
                    createAvatar(name, `${teamId}-${index}`, avatarStyle, membersContainer, teamId);
                }
            });

            teamContainer.appendChild(teamTitle);
            teamContainer.appendChild(membersContainer);
            teamContainers.appendChild(teamContainer);
        }
    });
});

// Fonction pour créer un avatar pour un élève
function createAvatar(name, studentId, avatarStyle, container, teamId) {
    if (!students[studentId]) {
        students[studentId] = {
            name: name,
            time: 0,
            intervalId: null,
            teamId: teamId  // Assigner un ID d'équipe à chaque élève
        };
    }

    const avatarDiv = document.createElement('div');
    avatarDiv.classList.add('avatarCard', 'm-4', 'cursor-pointer', 'text-center');
    avatarDiv.id = studentId;

    // Générer un avatar en fonction du style sélectionné
    const seed = Math.random().toString(36).substring(7);
    const avatarImg = document.createElement('img');
    avatarImg.src = getAvatarUrl(avatarStyle, seed); // Utiliser une fonction pour déterminer l'URL
    avatarImg.alt = `Avatar de ${name}`;
    avatarImg.classList.add('w-24', 'h-24', 'mx-auto', 'rounded-full', 'border-4', 'border-gray-300', 'transition', 'duration-300');

    const nameLabel = document.createElement('p');
    nameLabel.textContent = name;
    nameLabel.classList.add('mt-2', 'font-semibold');

    const timeLabel = document.createElement('div');
    timeLabel.classList.add('timeLabel', 'text-lg', 'text-gray-700', 'mt-1');
    timeLabel.textContent = '00:00';
    timeLabel.id = `time-${studentId}`;

    avatarDiv.appendChild(avatarImg);
    avatarDiv.appendChild(nameLabel);
    avatarDiv.appendChild(timeLabel);

    container.appendChild(avatarDiv);

    // Ajouter un écouteur d'événement pour gérer le clic sur l'avatar
    avatarDiv.addEventListener('click', () => handleAvatarClick(studentId, teamId));
}

// Fonction pour déterminer l'URL de l'avatar en fonction du style sélectionné
function getAvatarUrl(style, seed) {
    switch (style) {
        case 'avataaars-neutral':
            return `https://api.dicebear.com/9.x/avataaars-neutral/svg?seed=${seed}`;
        case 'bottts':
            return `https://api.dicebear.com/6.x/bottts/svg?seed=${seed}`;
        case 'bottts-9':
            return `https://api.dicebear.com/9.x/bottts/svg?seed=${seed}`;
        case 'croodles':
            return `https://api.dicebear.com/6.x/croodles/svg?seed=${seed}`;
        case 'croodles-neutral':
            return `https://api.dicebear.com/9.x/croodles-neutral/svg?seed=${seed}`;
        case 'fun-emoji':
            return `https://api.dicebear.com/9.x/fun-emoji/svg?seed=${seed}`;
        case 'pixel-art':
            return `https://api.dicebear.com/6.x/pixel-art/svg?seed=${seed}`;
        case 'thumbs':
            return `https://api.dicebear.com/9.x/thumbs/svg?seed=${seed}`;
        case 'adventurer':
        default:
            return `https://api.dicebear.com/6.x/adventurer/svg?seed=${seed}&gender=neutral`;
    }
}


// Fonction de réinitialisation des chronomètres
document.getElementById('resetButton').addEventListener('click', () => {
    // Réinitialiser les temps de parole individuels
    Object.keys(students).forEach(studentId => {
        clearInterval(students[studentId].intervalId);
        students[studentId].time = 0;
        students[studentId].intervalId = null;
        document.getElementById(`time-${studentId}`).textContent = '00:00';
        document.getElementById(studentId).querySelector('img').classList.remove('border-green-500');
        document.getElementById(studentId).querySelector('img').classList.add('border-gray-300');
    });

    // Réinitialiser les temps cumulés par équipe
    Object.keys(teamTimes).forEach(teamId => {
        teamTimes[teamId] = 0;
        updateTeamTime(teamId);
    });

    // Réinitialiser le chronomètre principal
    generalTime = 0;
    clearInterval(generalTimerInterval);
    document.getElementById('generalTimer').textContent = '00:00';
    generalTimerStarted = false; // Réinitialiser le flag pour permettre le redémarrage

    // Relancer le chronomètre principal si nécessaire
    startGeneralTimer();
});

// Fonction pour gérer le clic sur les avatars (chronomètres individuels)
function handleAvatarClick(studentId, teamId) {
    // Si c'est le premier démarrage de chronomètre après réinitialisation, relancer le chronomètre général
    if (!generalTimerStarted) {
        startGeneralTimer();  // Démarrer le chronomètre général si nécessaire
    }

    // Arrêter le chronomètre en cours s'il y en a un
    if (currentStudentId !== null && students[currentStudentId]) {
        clearInterval(students[currentStudentId].intervalId);
        students[currentStudentId].intervalId = null;

        // Retirer la bordure verte de l'avatar précédent
        document.getElementById(currentStudentId).querySelector('img').classList.remove('border-green-500');
        document.getElementById(currentStudentId).querySelector('img').classList.add('border-gray-300');
    }

    // Si on clique sur le même élève, on arrête simplement le chronomètre
    if (currentStudentId === studentId) {
        currentStudentId = null;
        checkChronometers();  // Vérifier l'état des chronos après chaque clic
        return;
    }

    currentStudentId = studentId;

    // Démarrer le chronomètre pour le nouvel élève
    const startTime = Date.now() - students[studentId].time;
    students[studentId].intervalId = setInterval(() => {
        students[studentId].time = Date.now() - startTime;
        document.getElementById(`time-${studentId}`).textContent = formatTime(students[studentId].time);

        // Mettre à jour le temps de l'équipe
        teamTimes[teamId] += 1000;
        updateTeamTime(teamId);
    }, 1000);

    // Ajouter la bordure verte à l'avatar en cours
    document.getElementById(studentId).querySelector('img').classList.add('border-green-500');
    document.getElementById(studentId).querySelector('img').classList.remove('border-gray-300');

    checkChronometers();  // Vérifier l'état des chronos après chaque clic
}


// Fonction pour démarrer le chronomètre général
function startGeneralTimer() {
    if (!generalTimerStarted) {
        generalTimerStarted = true;
        generalTimerInterval = setInterval(() => {
            generalTime += 1000;
            document.getElementById('generalTimer').textContent = formatTime(generalTime);
        }, 1000);
    }
}




// Fonction pour mettre à jour le temps total de l'équipe
function updateTeamTime(teamId) {
    const teamContainer = document.getElementById(teamId);
    const teamTitle = teamContainer.querySelector('h2');
    teamTitle.textContent = `Équipe ${teamId.split('-')[1]} - Temps cumulé : ${formatTime(teamTimes[teamId])}`;
}

// Fonction pour formater le temps
function formatTime(ms) {
    const totalSeconds = Math.floor(ms / 1000);
    const minutes = String(Math.floor(totalSeconds / 60)).padStart(2, '0');
    const seconds = String(totalSeconds % 60).padStart(2, '0');
    return `${minutes}:${seconds}`;
}






document.getElementById('exportButton').addEventListener('click', exportToCSV);

function exportToCSV() {
    let csvContent = "Prénom,Temps de parole,Équipe\n";

    // Exporter les élèves avec leur équipe et temps de parole
    Object.keys(students).forEach(studentId => {
        const student = students[studentId];
        const teamNumber = student.teamId.split('-')[1];  // Récupérer le numéro d'équipe
        csvContent += `${student.name},${formatTime(student.time)},Équipe ${teamNumber}\n`;
    });

    // Ajouter les temps cumulés par équipe
    csvContent += "\nTemps cumulé par équipe\n";
    Object.keys(teamTimes).forEach(teamId => {
        const teamNumber = teamId.split('-')[1];
        csvContent += `Équipe ${teamNumber},${formatTime(teamTimes[teamId])}\n`;
    });

    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    const link = document.createElement("a");
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "temps_de_parole_par_equipe.csv");
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}



let generalTimerInterval;  // Intervalle pour le chronomètre général
let generalTimerStarted = false;  // Indicateur pour savoir si le chronomètre général a déjà démarré
let generalTime = 0;  // Temps écoulé en millisecondes pour le chronomètre général

// Fonction pour démarrer le chronomètre général
function startGeneralTimer() {
    if (!generalTimerStarted) {
        generalTimerStarted = true;
        generalTimerInterval = setInterval(() => {
            generalTime += 1000;
            document.getElementById('generalTimer').textContent = formatTime(generalTime);
        }, 1000);
    }
}

// Fonction pour formater le temps du chronomètre général
function formatTime(ms) {
    const totalSeconds = Math.floor(ms / 1000);
    const minutes = String(Math.floor(totalSeconds / 60)).padStart(2, '0');
    const seconds = String(totalSeconds % 60).padStart(2, '0');
    return `${minutes}:${seconds}`;
}

// Modifier la fonction de démarrage des chronomètres individuels pour démarrer aussi le chronomètre général
function handleAvatarClick(studentId, teamId) {
    // Si c'est le premier démarrage de chronomètre, désactiver le bouton de génération et démarrer le chronomètre général
    if (!chronoLance) {
        chronoLance = true;
        document.getElementById('generateButton').disabled = true;
        document.getElementById('generateButton').classList.add('opacity-50', 'cursor-not-allowed');
        startGeneralTimer();  // Démarrer le chronomètre général
    }

    // Arrêter le chronomètre en cours s'il y en a un
    if (currentStudentId !== null && students[currentStudentId]) {
        clearInterval(students[currentStudentId].intervalId);
        students[currentStudentId].intervalId = null;

        // Retirer la classe d'indication visuelle
        document.getElementById(currentStudentId).querySelector('img').classList.remove('border-green-500');
        document.getElementById(currentStudentId).querySelector('img').classList.add('border-gray-300');
    }

    // Si on clique sur le même élève, on arrête simplement le chronomètre
    if (currentStudentId === studentId) {
        currentStudentId = null;
        return;
    }

    currentStudentId = studentId;

    // Démarrer le chronomètre pour le nouvel élève
    const startTime = Date.now() - students[studentId].time;
    students[studentId].intervalId = setInterval(() => {
        students[studentId].time = Date.now() - startTime;
        document.getElementById(`time-${studentId}`).textContent = formatTime(students[studentId].time);

        // Mettre à jour le temps de l'équipe
        teamTimes[teamId] += 1000;
        updateTeamTime(teamId);
    }, 1000);

    // Ajouter une indication visuelle pour l'élève en cours
    document.getElementById(studentId).querySelector('img').classList.add('border-green-500');
    document.getElementById(studentId).querySelector('img').classList.remove('border-gray-300');
}





// Désactiver le bouton synthèse tant que des chronos sont actifs
function checkChronometers() {
    const activeTimers = Object.values(students).some(student => student.intervalId !== null);
    
    if (activeTimers) {
        document.getElementById('summaryButton').disabled = true;  // Désactiver le bouton
        document.getElementById('summaryButton').classList.add('opacity-50', 'cursor-not-allowed');
    } else {
        document.getElementById('summaryButton').disabled = false;  // Activer le bouton
        document.getElementById('summaryButton').classList.remove('opacity-50', 'cursor-not-allowed');
    }
}

// Appeler cette fonction après chaque action qui peut changer l'état des chronos
function handleAvatarClick(studentId, teamId) {
    // Si c'est le premier démarrage de chronomètre, désactiver le bouton de génération
    if (!chronoLance) {
        chronoLance = true;
        document.getElementById('generateButton').disabled = true;
        document.getElementById('generateButton').classList.add('opacity-50', 'cursor-not-allowed');
        startGeneralTimer();  // Démarrer le chronomètre général
    }

    // Arrêter le chronomètre en cours s'il y en a un
    if (currentStudentId !== null && students[currentStudentId]) {
        clearInterval(students[currentStudentId].intervalId);
        students[currentStudentId].intervalId = null;

        // Retirer la classe d'indication visuelle
        document.getElementById(currentStudentId).querySelector('img').classList.remove('border-green-500');
        document.getElementById(currentStudentId).querySelector('img').classList.add('border-gray-300');
    }

    // Si on clique sur le même élève, on arrête simplement le chronomètre
    if (currentStudentId === studentId) {
        currentStudentId = null;
        checkChronometers();  // Vérifier l'état des chronos après chaque clic
        return;
    }

    currentStudentId = studentId;

    // Démarrer le chronomètre pour le nouvel élève
    const startTime = Date.now() - students[studentId].time;
    students[studentId].intervalId = setInterval(() => {
        students[studentId].time = Date.now() - startTime;
        document.getElementById(`time-${studentId}`).textContent = formatTime(students[studentId].time);

        // Mettre à jour le temps de l'équipe
        teamTimes[teamId] += 1000;
        updateTeamTime(teamId);
    }, 1000);

    // Ajouter une indication visuelle pour l'élève en cours
    document.getElementById(studentId).querySelector('img').classList.add('border-green-500');
    document.getElementById(studentId).querySelector('img').classList.remove('border-gray-300');

    checkChronometers();  // Vérifier l'état des chronos après chaque clic
}

// Générer la synthèse et arrêter le chrono principal
document.getElementById('summaryButton').addEventListener('click', () => {
    // Arrêter le chronomètre principal
    if (generalTimerStarted) {
        clearInterval(generalTimerInterval);  // Arrêter le chronomètre principal
        generalTimerStarted = false;  // Indiquer que le chronomètre principal est arrêté
    }

    // Générer la synthèse avec le graphique
    generateSummary();
});

function generateSummary() {
    const ctx = document.getElementById('summaryChart').getContext('2d');

    // Récupérer les noms des élèves et leurs temps
    const studentNames = Object.values(students).map(student => student.name);
    const studentTimes = Object.values(students).map(student => Math.floor(student.time / 1000));  // Temps en secondes

    // Récupérer les temps cumulés par équipe
    const teamNames = Object.keys(teamTimes).map(teamId => `Équipe ${teamId.split('-')[1]}`);
    const teamTotalTimes = Object.values(teamTimes).map(time => Math.floor(time / 1000));  // Temps en secondes

    // Créer le graphique avec Chart.js
    new Chart(ctx, {
        type: 'bar',  // Graphique à barres
        data: {
            labels: studentNames.concat(teamNames),  // Noms des élèves + Noms des équipes, équipes à la fin
            datasets: [
                {
                    label: 'Temps de parole des élèves (secondes)',
                    data: studentTimes.concat(Array(teamNames.length).fill(null)),  // Ajouter des "null" pour espacer les équipes
                    backgroundColor: '#36A2EB',  // Couleur pour les élèves
                    yAxisID: 'y',  // Assignation à l'axe primaire
                },
                {
                    label: 'Temps de parole par équipe (secondes)',
                    data: Array(studentNames.length).fill(null).concat(teamTotalTimes),  // Espacer les élèves des équipes
                    backgroundColor: '#FF6384',  // Couleur pour les équipes
                    yAxisID: 'y1',  // Assignation à l'axe secondaire
                }
            ]
        },
        options: {
            responsive: true,
            scales: {
                y: {  // Axe primaire pour les élèves
                    beginAtZero: true,
                    position: 'left',
                    title: {
                        display: true,
                        text: 'Temps des élèves (secondes)'
                    }
                },
                y1: {  // Axe secondaire pour les équipes
                    beginAtZero: true,
                    position: 'right',
                    title: {
                        display: true,
                        text: 'Temps des équipes (secondes)'
                    },
                    grid: {
                        drawOnChartArea: false  // Empêcher la superposition des grilles des deux axes
                    }
                }
            },
            plugins: {
                legend: {
                    display: true
                },
                title: {
                    display: true,
                    text: 'Synthèse du temps de parole : Élèves vs Équipes'
                }
            }
        }
    });
}


document.getElementById('helpButton').addEventListener('click', function() {
    introJs().setOptions({
        steps: [
            { 
                intro: "Bienvenue sur Chronoclasse ! Voyons comment fonctionne l'application." 
            },
            { 
                element: document.querySelector('#nameInput'),
                intro: "Saisissez les prénoms des élèves ici, séparés par des virgules. Utilisez un slash '/' pour séparer les équipes en cas de débat."
            },
            {
                element: document.querySelector('#avatarStyle'),
                intro: "Choisissez un style d'avatar pour les participants."
            },
            {
                element: document.querySelector('#generateButton'),
                intro: "Cliquez ici pour générer les avatars avec les prénoms que vous avez saisis."
            },
            {
                element: document.querySelector('#teamsContainer'),
                intro: "Pour démarrer ou arrêter un chronomètre, cliquez simplement sur l'avatar de l'élève. Le temps de parole de l'élève sera automatiquement enregistré."
            },
            {
                element: document.querySelector('#resetButton'),
                intro: "Réinitialisez tous les chronomètres à zéro."
            },
            {
                element: document.querySelector('#summaryButton'),
                intro: "Terminez l'activité et visualisez la synthèse du temps de parole sous forme de graphique."
            }
        ],
        nextLabel: 'Suivant',
        prevLabel: 'Précédent',
        doneLabel: 'Terminer'
    }).start();
});
